{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLists #-}

module Main where

import Control.Monad.IO.Class (MonadIO(..))
import Control.Monad.Managed (Managed, managed_, runManaged)
import Say (sayString)
import System.Environment (getArgs)

import qualified Data.List.NonEmpty as NonEmpty
import qualified Data.Vector as Vector
import qualified SDL
import qualified Vulkan.Core10 as Core10
import qualified VulkanMemoryAllocator as VMA

import Types

import qualified Action
import qualified Lib
import qualified MainLoop
import qualified Scene
import qualified Shaders

main :: IO ()
main = runManaged $ do
  managed_ Lib.withSDL

  args <- liftIO getArgs
  let
    layers = mconcat
      [ [ "VK_LAYER_LUNARG_standard_validation" ]
      , [ "VK_LAYER_RENDERDOC_Capture"
        | "--renderdoc" `elem` args
        ]
      ]

  vw <- Lib.withVulkanWindow "vulkan tuts" Scene.windowWidth Scene.windowHeight layers
  initialSync <- createSyncObjects vw

  renderPass <- Lib.createRenderPass vw
  (pipeline, descs) <- Lib.createGraphicsPipeline vw renderPass Shaders.create
  framebuffers <- Lib.createFramebuffers vw renderPass

  commandBuffers <- Lib.createCommandBuffers
    vw
    renderPass
    pipeline
    framebuffers
    Action.setup
    Action.commands

  SDL.showWindow (vwSdlWindow vw)

  let
    draw =
      MainLoop.drawFrame vw $
        Vector.zip descs commandBuffers

  liftIO $ MainLoop.mainLoop draw initialSync

  Core10.deviceWaitIdle (vwDevice vw)

  stats <- VMA.calculateStats (vwAllocator vw)
  sayString $ "Allocator stats: " <> show (VMA.total stats)

createSyncObjects :: VulkanWindow -> Managed SyncObjects
createSyncObjects vw =
  fmap (NonEmpty.cycle . NonEmpty.fromList) $
    traverse (Lib.createSyncObject vw) [0..MainLoop.INFLIGHT_FRAMES]
