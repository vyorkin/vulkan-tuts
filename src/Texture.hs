module Texture where

import Control.Exception (bracket, bracket_)
import Foreign (Ptr)
import Linear (V2(..))

import qualified SDL
import qualified SDL.Image

loadImage :: Integral size => FilePath -> (size -> size -> Ptr () -> IO a) -> IO a
loadImage filePath action =
  bracket (SDL.Image.load filePath) SDL.freeSurface $ \source -> do
    V2 width height <- SDL.surfaceDimensions source
    let sdlSize = V2 width height
    bracket (SDL.createRGBSurface sdlSize SDL.RGBA8888) SDL.freeSurface $ \temporary -> do
      _nothing <- SDL.surfaceBlit source Nothing temporary Nothing
      bracket_ (SDL.lockSurface temporary) (SDL.unlockSurface temporary) $ do
        pixels <- SDL.surfacePixels temporary
        action (fromIntegral width) (fromIntegral height) pixels
