{-# LANGUAGE OverloadedLists #-}

module Buffer where

import Control.Exception (bracket)
import Data.Bits ((.|.))
import Data.Word (Word16)
import Vulkan.Core10 as Core10
import Vulkan.NamedType ((:::))
import Vulkan.Zero (zero)

import qualified Foreign
import qualified VulkanMemoryAllocator as VMA

import Buffer.Commands (oneshot)
import Types (UBO(..), VulkanWindow(..), Vertex(..), vertexItems)

-- * Vertex buffer

withVertices
  :: VulkanWindow
  -> [Vertex]
  -> (Buffer -> IO c)
  -> IO c
withVertices vw vertices proc = bracket create destroy (proc . fst)
  where
    create = do
      (vertex, allocation, _info) <- VMA.createBuffer vwAllocator vertexBufferCI vertexAllocationCI

      VMA.withBuffer vwAllocator stageBufferCI stageAllocationCI bracket $ \(staging, stage, _stageInfo) -> do
        VMA.withMappedMemory vwAllocator stage bracket $ \ptr -> do
          Foreign.pokeArray (Foreign.castPtr ptr) items
          VMA.flushAllocation vwAllocator stage 0 Core10.WHOLE_SIZE

        copyBuffer vw staging vertex itemsSize

      pure (vertex, allocation)

    destroy (buf, alloc) = VMA.destroyBuffer vwAllocator buf alloc

    VulkanWindow{vwAllocator} = vw

    (items, itemsSize) = vertexItems vertices

    stageBufferCI :: BufferCreateInfo '[]
    stageBufferCI = zero
      { size        = itemsSize
      , usage       = BUFFER_USAGE_TRANSFER_SRC_BIT
      , sharingMode = SHARING_MODE_EXCLUSIVE
      }

    stageAllocationCI :: VMA.AllocationCreateInfo
    stageAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_CPU_TO_GPU
      , VMA.requiredFlags = Core10.MEMORY_PROPERTY_HOST_VISIBLE_BIT
          -- .|. Core10.MEMORY_PROPERTY_HOST_COHERENT_BIT
      }

    vertexBufferCI :: BufferCreateInfo '[]
    vertexBufferCI = zero
      { size        = itemsSize
      , usage       = BUFFER_USAGE_TRANSFER_DST_BIT .|. BUFFER_USAGE_VERTEX_BUFFER_BIT
      , sharingMode = SHARING_MODE_EXCLUSIVE
      }

    vertexAllocationCI :: VMA.AllocationCreateInfo
    vertexAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_CPU_TO_GPU
      , VMA.requiredFlags = Core10.MEMORY_PROPERTY_DEVICE_LOCAL_BIT
      }

-- * Index buffer

withIndices :: VulkanWindow -> [Word16] -> (Buffer -> IO c) -> IO c
withIndices vw indices proc = bracket create destroy (proc . fst)
  where
    create = createIndexBuffer vw indices

    destroy (buf, alloc) = VMA.destroyBuffer (vwAllocator vw) buf alloc

createIndexBuffer :: VulkanWindow -> [Word16] -> IO (Buffer, VMA.Allocation)
createIndexBuffer vw indices = do
  (buf, allocation, _info) <- VMA.createBuffer vwAllocator indexBufferCI indexAllocationCI

  VMA.withBuffer vwAllocator stageBufferCI stageAllocationCI bracket $ \(staging, stage, _stageInfo) -> do
    VMA.withMappedMemory vwAllocator stage bracket $ \ptr -> do
      Foreign.pokeArray (Foreign.castPtr ptr) indices
      VMA.flushAllocation vwAllocator stage 0 Core10.WHOLE_SIZE

    copyBuffer vw staging buf itemsSize

  pure (buf, allocation)
  where
    VulkanWindow{..} = vw

    itemsSize = fromIntegral $ Foreign.sizeOf (head indices) * length indices

    stageBufferCI :: BufferCreateInfo '[]
    stageBufferCI = zero
      { size        = itemsSize
      , usage       = BUFFER_USAGE_TRANSFER_SRC_BIT
      , sharingMode = SHARING_MODE_EXCLUSIVE
      }

    stageAllocationCI :: VMA.AllocationCreateInfo
    stageAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_CPU_TO_GPU
      , VMA.requiredFlags = Core10.MEMORY_PROPERTY_HOST_VISIBLE_BIT
      }

    indexBufferCI :: BufferCreateInfo '[]
    indexBufferCI = zero
      { size        = itemsSize
      , usage       = BUFFER_USAGE_TRANSFER_DST_BIT .|. BUFFER_USAGE_INDEX_BUFFER_BIT
      , sharingMode = SHARING_MODE_EXCLUSIVE
      }

    indexAllocationCI :: VMA.AllocationCreateInfo
    indexAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_CPU_TO_GPU
      , VMA.requiredFlags = Core10.MEMORY_PROPERTY_DEVICE_LOCAL_BIT
      }

-- * Uniform buffer

withUniform :: Foreign.Storable a => VulkanWindow -> a -> (UBO a -> IO c) -> IO c
withUniform vw initial proc = bracket create destroy proc
  where
    create =
      createUniform vw initial

    destroy UBO{..} =
      VMA.destroyBuffer (vwAllocator vw) uboBuffer uboAllocation

createUniform :: Foreign.Storable a => VulkanWindow -> a -> IO (UBO a)
createUniform vw@VulkanWindow{..} initial = do
  (buf, allocation, _info) <- VMA.createBuffer vwAllocator uboCI uboAllocate

  let
    ubo = UBO
      { uboBuffer     = buf
      , uboAllocation = allocation
      -- , uboUpdate = mkUpdate buf allocation
      }

  updateUniform vw ubo initial

  pure ubo

  where
    uboSize = fromIntegral $ Foreign.sizeOf initial

    uboCI :: Core10.BufferCreateInfo '[]
    uboCI = zero
      { size        = uboSize
      , usage       = Core10.BUFFER_USAGE_UNIFORM_BUFFER_BIT
      , sharingMode = SHARING_MODE_EXCLUSIVE
      }

    uboAllocate :: VMA.AllocationCreateInfo
    uboAllocate = zero
      { VMA.usage =
          VMA.MEMORY_USAGE_CPU_TO_GPU
      , VMA.requiredFlags =
          Core10.MEMORY_PROPERTY_HOST_VISIBLE_BIT .|.
          Core10.MEMORY_PROPERTY_HOST_COHERENT_BIT
      }

updateUniform :: Foreign.Storable a => VulkanWindow -> UBO a -> a -> IO ()
updateUniform VulkanWindow{vwAllocator} UBO{..} value = do
  -- putStrLn "updating UBO"
  VMA.withMappedMemory vwAllocator uboAllocation bracket $ \ptr ->
    Foreign.poke (Foreign.castPtr ptr) value

-- * Staging buffer utils

copyBuffer
  :: VulkanWindow
  -> ("src" ::: Buffer)
  -> ("dst" ::: Buffer)
  -> DeviceSize
  -> IO ()
copyBuffer vw src dst size =
  oneshot vw $ \buf ->
    Core10.cmdCopyBuffer buf src dst [Core10.BufferCopy 0 0 size]
