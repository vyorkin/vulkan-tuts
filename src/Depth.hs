module Depth where

import Control.Monad.IO.Class (MonadIO(..))
import Control.Monad.Managed (Managed)
import Data.Traversable (for)
import Vulkan.Zero (zero)

import qualified Vulkan.Core10 as Core10
import qualified VulkanMemoryAllocator as VMA

import Types (VulkanWindow(..), (.&&.), allocate)

import qualified Buffer.Image
import qualified Image

getOptimalDepthFormat :: MonadIO m => Core10.PhysicalDevice -> m Core10.Format
getOptimalDepthFormat device = do
  formatProperties <- for candidates $ \fmt -> do
    props <- Core10.getPhysicalDeviceFormatProperties device fmt
    pure
      ( Core10.optimalTilingFeatures props .&&. Core10.FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
      , fmt
      )
  case map snd $ filter fst formatProperties of
    [] ->
      error $ "No depth buffer format available from candidates: " <> show candidates
    pick : _rest ->
      pure pick

  where
    candidates =
      [ Core10.FORMAT_D32_SFLOAT
      , Core10.FORMAT_D32_SFLOAT_S8_UINT
      , Core10.FORMAT_D24_UNORM_S8_UINT
      ]

createDepthTextureImage :: VulkanWindow -> Managed Core10.ImageView
createDepthTextureImage vw = do
  liftIO $ print extent
  (image, _allocation, _info) <- VMA.withImage (vwAllocator vw) imageCI imageAllocationCI allocate
  imageView <- Image.withImageView
    allocate
    (vwDevice vw)
    (vwDepthFormat vw)
    Core10.IMAGE_ASPECT_DEPTH_BIT
    zero
    1
    image

  -- XXX: tutorial says it is optional
  liftIO $ Buffer.Image.transitionImageLayout
    vw
    image
    1
    (vwDepthFormat vw)
    Core10.IMAGE_LAYOUT_UNDEFINED
    Core10.IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
  pure imageView
  where
    imageCI = zero
      { Core10.imageType     = Core10.IMAGE_TYPE_2D
      , Core10.format        = vwDepthFormat vw
      , Core10.extent        = extent
      , Core10.mipLevels     = 1
      , Core10.arrayLayers   = 1
      , Core10.samples       = vwMsaaSamples vw
      , Core10.tiling        = Core10.IMAGE_TILING_OPTIMAL
      , Core10.usage         = Core10.IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT
      , Core10.sharingMode   = Core10.SHARING_MODE_EXCLUSIVE
      , Core10.initialLayout = Core10.IMAGE_LAYOUT_UNDEFINED
      }

    extent = Core10.Extent3D
      (Core10.width  (vwExtent vw :: Core10.Extent2D))
      (Core10.height (vwExtent vw :: Core10.Extent2D))
      1

    imageAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_GPU_ONLY
      , VMA.requiredFlags = Core10.MEMORY_PROPERTY_DEVICE_LOCAL_BIT
      }
