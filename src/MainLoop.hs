{-# LANGUAGE OverloadedLists #-}

module MainLoop where

import Data.List.NonEmpty (NonEmpty((:|)))
import Data.Vector (Vector)
import Linear (V3(..))
import Vulkan.CStruct.Extends (SomeStruct(..))
import Vulkan.Extensions.VK_KHR_swapchain (PresentInfoKHR(..), acquireNextImageKHR, queuePresentKHR)
import Vulkan.Zero (zero)

import qualified Data.Vector as Vector
import qualified Vulkan.Core10 as Core10
import qualified SDL
import qualified Linear

import Types

import qualified Buffer
import qualified Scene

-- Intel says:
--
--     Having two sets of frame resources is a must.
--     Increasing the number of frame resources to three
--     doesn't give us much more performance (if any at all).
--
-- https://software.intel.com/en-us/articles/practical-approach-to-vulkan-part-1
pattern INFLIGHT_FRAMES :: Int
pattern INFLIGHT_FRAMES = 2

mainLoop :: (SyncObject -> IO ()) -> SyncObjects -> IO ()
mainLoop draw (current :| next) = do
  events <- SDL.pollEvents
  if any isQuitEvent events then
    pure ()
  else do
    draw current
    mainLoop draw $
      case next of
        []      -> [current]
        n : ext -> n :| ext
  where
    isQuitEvent :: SDL.Event -> Bool
    isQuitEvent = \case
      (SDL.Event _ SDL.QuitEvent) -> True
      SDL.Event _ (SDL.KeyboardEvent (SDL.KeyboardEventData _ SDL.Released False (SDL.Keysym _ code _)))
        | code == SDL.KeycodeQ || code == SDL.KeycodeEscape
        -> True
      _ -> False

drawFrame :: VulkanWindow -> Vector (UBO (V3 Mat4), Core10.CommandBuffer) -> SyncObject -> IO ()
drawFrame vw@VulkanWindow{..} perImage SyncObject{..} = do
  Core10.waitForFences vwDevice [soInFlightFence] True maxBound >>= \case
    Core10.SUCCESS ->
      pure ()
    err ->
      error $ "waitForFences failed: " <> show err

  forImage <- acquireNextImageKHR vwDevice vwSwapchain maxBound soImageAvailable zero >>= \case
    (Core10.SUCCESS, imageIndex) ->
      case perImage Vector.!? fromIntegral imageIndex of
        Nothing ->
          error ""
        Just res ->
          pure (imageIndex, res)
    (err, imageIndex) ->
      error $ "acquireNextImageKHR: " <> show err <> " for image " <> show imageIndex

  let (imageIndex, (mvp, commandBuffer)) = forImage

  ms <- fmap fromIntegral SDL.ticks
  let
    cameraTarget = V3 0 0 (-0.25)
    cameraOrigin = V3 x y z
    fov = 50 + 10 * sin (seconds / 2.5)

    Linear.V2 x y = Linear.angle (0.5 + 0.5 * sin (seconds / 2)) Linear.^* 4
    z = 0.5 * sin (seconds / 3) - 1

    seconds = ms / 1000

  Buffer.updateUniform vw mvp $ V3
    Linear.identity
    (Scene.looking cameraOrigin cameraTarget)
    (Scene.perspective fov 0.1 10)

  let
    submitInfo = SomeStruct zero
      { Core10.waitSemaphores   = [soImageAvailable]
      , Core10.waitDstStageMask = [Core10.PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT]
      , Core10.commandBuffers   = [Core10.commandBufferHandle commandBuffer]
      , Core10.signalSemaphores = [soRenderFinished]
      }

  Core10.resetFences vwDevice [soInFlightFence]
  Core10.queueSubmit vwGraphicsQueue [submitInfo] soInFlightFence

  let
    presentInfo = zero
      { waitSemaphores = [soRenderFinished]
      , swapchains     = [vwSwapchain]
      , imageIndices   = [imageIndex]
      }
  _result <- queuePresentKHR vwPresentQueue presentInfo

  pure ()
