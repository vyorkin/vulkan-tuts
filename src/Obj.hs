module Obj where

import Codec.Wavefront -- (WavefrontOBJ(..), fromFile)
import Control.Arrow ((&&&))
import Linear (V2(..), V3(..))

import qualified Data.Vector as Vector

import Types (Indexed(..), Vertex(..), indexed)

loadObj :: FilePath -> IO (Indexed Vertex)
loadObj fp = fromFile fp >>= either fail (pure . toIndexed)

toIndexed :: WavefrontOBJ -> Indexed Vertex
toIndexed WavefrontOBJ{..} = indexed vertices
  where
    vertices = do
      Element{elValue=(Triangle a b c)} <- Vector.toList objFaces -- BUG: quads are ignored
      (loc, mtex) <- map (faceLocIndex &&& faceTexCoordIndex) [a, b, c]

      let Location x y z _w = objLocations Vector.! (loc - 1)

      let
        (u, v) = case mtex of
          Nothing ->
            (0, 0)
          Just ti ->
            let
              TexCoord tu tv _tw = objTexCoords Vector.! (ti - 1)
            in
              (tu, tv)

      pure Vertex
        { vPos      = V3 x y (negate z)
        , vColor    = V3 2 0.9 0.9
        , vTexCoord = V2 u (1 - v)
        }
