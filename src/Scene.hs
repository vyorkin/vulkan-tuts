module Scene where

import Control.Lens ((&), (.~))
import Linear (V2(..), V3(..), V4(..))

import qualified Linear

import Types (Indexed(..), Vertex(..), Mat4)

windowWidth, windowHeight :: Int
windowWidth = 1024
windowHeight = 768

compo :: [Vertex]
compo = mconcat $ zlayers [0, -0.15 ..]
  [ squareUV $ wheel 0.9 0 $ replicate 4 1
  , squareUV $ wheel 0.4 1 $ replicate 6 0
  , squareUV $ wheel 0.33 1
      [ V3 1 0 0
      , V3 1 1 0
      , V3 0 1 0
      , V3 0 1 1
      , V3 0 0 1
      , V3 1 0 1
      ]
  ]

zlayers :: [Float] -> [[Vertex]] -> [[Vertex]]
zlayers zs layers = do
  (z, layer) <- zip zs layers
  pure $ do
    v@Vertex{vPos} <- layer
    pure v
      { vPos = vPos + V3 0 0 z
      }

squareUV :: [Vertex] -> [Vertex]
squareUV = \case
  [] -> []
  input@(first : rest) -> do
    v@Vertex{vPos=V3 x y _z} <- input
    pure v
      { vTexCoord = V2
          (x / (maxX - minX) + 0.5)
          (y / (maxY - minY) + 0.5)
      }
    where
      (minX, maxX, minY, maxY) = foldr go (ix, ix, iy, iy) rest

      go Vertex{vPos=(V3 x y _z)} (xi, xa, yi, ya) =
        ( min x xi
        , max x xa
        , min y yi
        , max y ya
        )

      V3 ix iy _iz = vPos first

wheel :: Float -> V3 Float -> [V3 Float] -> [Vertex]
wheel size central colors = do
  (cur, next) <- zip corners (drop 1 $ cycle corners)
  [o, cur, next]
  where
    sizeX = size
    sizeY = size

    sides = length colors
    sideRads = 2 * pi / fromIntegral sides

    o = Vertex
      { vPos      = 0
      , vColor    = central
      , vTexCoord = V2 0 0
      }

    corners = do
      (ix, color) <- zip [0..] colors
      pure $ Vertex (corner ix) color (V2 0 0)

    corner ix = V3
      (sizeX * cos (ix * sideRads))
      (sizeY * sin (ix * sideRads))
      0

square2 :: Indexed Vertex
square2 = Indexed vertices indices
  where
    vertices =
      [ Vertex leftTop     (V3 1 0 0) (V2 0 0)
      , Vertex rightTop    (V3 0 0 1) (V2 1 0)
      , Vertex rightBottom (V3 0 1 0) (V2 1 1)
      , Vertex leftBottom  (V3 1 1 1) (V2 0 1)
      ]

    indices =
      [ 0, 1, 2
      , 2, 3, 0
      ]

    leftTop     = V3 (-0.5) (-0.5) 0
    rightTop    = V3 ( 0.5) (-0.5) 0
    rightBottom = V3 ( 0.5) ( 0.5) 0
    leftBottom  = V3 (-0.5) ( 0.5) 0

translate :: V3 Float -> Mat4
translate v3 =
  Linear.transpose $ Linear.identity
    & Linear.translation .~ v3

perspective :: Float -> Float -> Float -> Mat4
perspective fovDegs near far = V4
  (V4 (f * aspectX)          0                           0     0)
  (V4             0 (negate f)                           0     0)
  (V4             0          0        (far / (near - far))  (-1))
  (V4             0          0 (near * far / (near - far)) fixup)
  where
    f = recip . tan $ 0.5 * fovDegs / 180 * pi
    fixup = 0 -- 1 -- XXX: the source had this at 0, but so:55480918 tells it should be 1

looking :: V3 Float -> V3 Float -> Mat4
looking from to = Linear.transpose $ Linear.lookAt from to up
  where
    up :: V3 Float
    up = V3 0 0 (-1)

aspectX :: Float
aspectX =
  if windowWidth > windowHeight then
    fromIntegral windowHeight / fromIntegral windowWidth
  else
    1.0

aspectY :: Float
aspectY =
  if windowWidth > windowHeight then
    1.0
  else
    fromIntegral windowWidth / fromIntegral windowHeight
