{-# LANGUAGE OverloadedLists #-}

module Buffer.Image where

import Control.Exception (bracket)
import Data.Bits ((.|.))
import Data.Foldable (for_)
import Data.Word (Word32)
import Vulkan.Core10 as Core10
import Vulkan.CStruct.Extends (SomeStruct(..))
import Vulkan.NamedType ((:::))
import Vulkan.Zero (zero)

import qualified Foreign
import qualified VulkanMemoryAllocator as VMA

import Types (VulkanWindow(..))
import Buffer.Commands (oneshot)

import qualified Texture
import qualified Image

-- * Image buffer

createImage :: VulkanWindow -> FilePath -> IO (Image, "mipLevels" ::: Word32, VMA.Allocation)
createImage vw path =
  Texture.loadImage path $ \width height pixelsPtr -> do
    let pixelBytes = fromIntegral $ width * height * 4
    let extent = Core10.Extent3D width height 1
    let mipLevels = Image.log2MipLevels extent

    print ((width, height), (pixelsPtr, pixelBytes))

    (image, allocation, _info) <- VMA.createImage vwAllocator (imageCI extent) imageAllocationCI

    VMA.withBuffer vwAllocator (stageBufferCI pixelBytes) stageAllocationCI bracket $ \(staging, stage, _stageInfo) -> do
      VMA.withMappedMemory vwAllocator stage bracket $ \stagePtr ->
        Foreign.copyBytes stagePtr pixelsPtr pixelBytes

      putStrLn "Image transition started"

      transitionImageLayout
        vw
        image
        mipLevels
        Core10.FORMAT_R8G8B8A8_SRGB
        Core10.IMAGE_LAYOUT_UNDEFINED
        Core10.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL

      putStrLn "Image transfer started"

      copyBufferToImage vw staging image extent

      putStrLn "Image transfer ended"

      -- XXX: converting Word32 sizes to Int32 offsets, huh?
      generateMipmaps vw image (fromIntegral width) (fromIntegral height) mipLevels

      putStrLn "Image mipmaps generated"

    pure (image, mipLevels, allocation)
  where
    VulkanWindow{..} = vw

    usage =
      Core10.IMAGE_USAGE_SAMPLED_BIT .|.      -- Sampler
      Core10.IMAGE_USAGE_TRANSFER_DST_BIT .|. -- Staging
      Core10.IMAGE_USAGE_TRANSFER_SRC_BIT     -- Mip generation

    imageCI :: Core10.Extent3D -> Core10.ImageCreateInfo '[]
    imageCI extent = zero
      { imageType     = IMAGE_TYPE_2D
      , format        = Core10.FORMAT_R8G8B8A8_SRGB
      , extent        = extent
      , mipLevels     = Image.log2MipLevels extent
      , arrayLayers   = 1
      , tiling        = Core10.IMAGE_TILING_OPTIMAL
      , initialLayout = Core10.IMAGE_LAYOUT_UNDEFINED
      , usage         = usage
      , sharingMode   = Core10.SHARING_MODE_EXCLUSIVE
      , samples       = SAMPLE_COUNT_1_BIT -- XXX: no multisampling here
      }

    imageAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_CPU_TO_GPU
      , VMA.requiredFlags = Core10.MEMORY_PROPERTY_DEVICE_LOCAL_BIT
      }

    stageBufferCI :: Int -> BufferCreateInfo '[]
    stageBufferCI pixelBytes = zero
      { size        = fromIntegral pixelBytes
      , usage       = BUFFER_USAGE_TRANSFER_SRC_BIT
      , sharingMode = SHARING_MODE_EXCLUSIVE
      }

    stageAllocationCI :: VMA.AllocationCreateInfo
    stageAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_CPU_TO_GPU
      , VMA.requiredFlags = Core10.MEMORY_PROPERTY_HOST_VISIBLE_BIT
      }

destroyImage :: VulkanWindow -> (Image, "mipLevels" ::: Word32, VMA.Allocation) -> IO ()
destroyImage vw (image, _mipLevels, allocation) = VMA.destroyImage (vwAllocator vw) image allocation

transitionImageLayout
  :: VulkanWindow
  -> Core10.Image
  -> "mipLevels" ::: Word32
  -> Core10.Format
  -> ("old" ::: Core10.ImageLayout)
  -> ("new" ::: Core10.ImageLayout)
  -> IO ()
transitionImageLayout vw image mipLevels format old new =
  oneshot vw $ \buf ->
    case (old, new) of
      (Core10.IMAGE_LAYOUT_UNDEFINED, Core10.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) ->
        Core10.cmdPipelineBarrier
          buf
          Core10.PIPELINE_STAGE_TOP_OF_PIPE_BIT
          Core10.PIPELINE_STAGE_TRANSFER_BIT
          zero
          mempty
          mempty
          [ barrier Core10.IMAGE_ASPECT_COLOR_BIT zero Core10.ACCESS_TRANSFER_WRITE_BIT
          ]
      (Core10.IMAGE_LAYOUT_UNDEFINED, Core10.IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) ->
        Core10.cmdPipelineBarrier
          buf
          Core10.PIPELINE_STAGE_TOP_OF_PIPE_BIT
          Core10.PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT
          zero
          mempty
          mempty
          [ barrier aspectMask zero $
              Core10.ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT .|.
              Core10.ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT
          ]
        where
          aspectMask =
            if hasStencilComponent then
              Core10.IMAGE_ASPECT_DEPTH_BIT .|. Core10.IMAGE_ASPECT_STENCIL_BIT
            else
              Core10.IMAGE_ASPECT_DEPTH_BIT
          hasStencilComponent =
            format == Core10.FORMAT_D32_SFLOAT_S8_UINT ||
            format == Core10.FORMAT_D24_UNORM_S8_UINT

      (Core10.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, Core10.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) ->
        Core10.cmdPipelineBarrier
          buf
          Core10.PIPELINE_STAGE_TRANSFER_BIT
          Core10.PIPELINE_STAGE_FRAGMENT_SHADER_BIT
          zero
          mempty
          mempty
          [ barrier Core10.IMAGE_ASPECT_COLOR_BIT Core10.ACCESS_TRANSFER_WRITE_BIT Core10.ACCESS_SHADER_READ_BIT
          ]
      _ ->
        error $ "Unsupported image layout transfer: " <> show (old, new)
  where
    barrier aspectMask srcMask dstMask = SomeStruct zero
      { srcAccessMask       = srcMask
      , dstAccessMask       = dstMask
      , oldLayout           = old
      , newLayout           = new
      , srcQueueFamilyIndex = Core10.QUEUE_FAMILY_IGNORED
      , dstQueueFamilyIndex = Core10.QUEUE_FAMILY_IGNORED
      , image               = image
      , subresourceRange    = Image.subresource aspectMask mipLevels
      }

copyBufferToImage
  :: VulkanWindow
  -> Core10.Buffer
  -> Core10.Image
  -> Core10.Extent3D
  -> IO ()
copyBufferToImage vw src dst extent =
  oneshot vw $ \buf ->
    Core10.cmdCopyBufferToImage buf src dst Core10.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL [region]
  where
    region = zero
      { imageSubresource = Core10.ImageSubresourceLayers
          { aspectMask     = Core10.IMAGE_ASPECT_COLOR_BIT
          , mipLevel       = 0
          , baseArrayLayer = 0
          , layerCount     = 1
          }
      , imageExtent = extent
      }

generateMipmaps :: VulkanWindow -> Image -> Foreign.Int32 -> Foreign.Int32 -> Word32 -> IO ()
generateMipmaps _vw image _width _height 0 =
  putStrLn $ "Skipping mipmaps " <> show image
generateMipmaps vw image width height mipLevels = do
  putStrLn $ "Generating mipmaps " <> show (image, mipRange)
  oneshot vw $ \buf -> do
    for_ mipRange $ \baseLevel -> do
      let mipLevel = succ baseLevel
      let dims = (mipWidth mipLevel, mipHeight mipLevel)
      let baseSubr = subr baseLevel

      putStrLn $ "Transferring mip base: " <> show baseLevel
      cmdBarrier buf Core10.PIPELINE_STAGE_TRANSFER_BIT barrierBase
        { subresourceRange = baseSubr
        , oldLayout        = Core10.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
        , newLayout        = Core10.IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
        , srcAccessMask    = Core10.ACCESS_TRANSFER_WRITE_BIT
        , dstAccessMask    = Core10.ACCESS_TRANSFER_READ_BIT
        }

      putStrLn $ "Blitting " <> show (baseLevel, mipLevel, dims)
      Core10.cmdBlitImage
        buf
        image Core10.IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
        image Core10.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
        [blit $ succ baseLevel]
        Core10.FILTER_LINEAR

      putStrLn $ "Locking mip base: " <> show baseLevel
      cmdBarrier buf Core10.PIPELINE_STAGE_FRAGMENT_SHADER_BIT barrierBase
        { subresourceRange = baseSubr
        , oldLayout        = Core10.IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
        , newLayout        = Core10.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        , srcAccessMask    = Core10.ACCESS_TRANSFER_READ_BIT
        , dstAccessMask    = Core10.ACCESS_SHADER_READ_BIT
        }

    putStrLn $ "Locking last mip: " <> show lastLevel
    cmdBarrier buf Core10.PIPELINE_STAGE_FRAGMENT_SHADER_BIT barrierBase
      { subresourceRange = subr lastLevel
      , oldLayout        = Core10.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
      , newLayout        = Core10.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
      , srcAccessMask    = Core10.ACCESS_TRANSFER_WRITE_BIT
      , dstAccessMask    = Core10.ACCESS_SHADER_READ_BIT
      }
  where
    lastLevel = mipLevels - 1
    mipRange = [0 .. pred lastLevel] :: [Word32]

    cmdBarrier buf dstStage barrier = Core10.cmdPipelineBarrier
      buf
      Core10.PIPELINE_STAGE_TRANSFER_BIT
      dstStage
      zero
      mempty
      mempty
      [SomeStruct barrier]

    barrierBase = zero
      { srcQueueFamilyIndex = Core10.QUEUE_FAMILY_IGNORED
      , dstQueueFamilyIndex = Core10.QUEUE_FAMILY_IGNORED
      , image               = image
      }

    subr baseMip = Core10.ImageSubresourceRange
      { aspectMask     = Core10.IMAGE_ASPECT_COLOR_BIT
      , baseMipLevel   = baseMip
      , baseArrayLayer = 0
      , levelCount     = 1
      , layerCount     = 1
      }

    blit level = Core10.ImageBlit
      { srcSubresource = srcSubr
      , srcOffsets     = (zero, srcOffset)
      , dstSubresource = dstSubr
      , dstOffsets     = (zero, dstOffset)
      }
      where
        predLevel = pred level

        srcSubr = Core10.ImageSubresourceLayers
          { aspectMask     = Core10.IMAGE_ASPECT_COLOR_BIT
          , mipLevel       = predLevel
          , baseArrayLayer = 0
          , layerCount     = 1
          }
        dstSubr = Core10.ImageSubresourceLayers
          { aspectMask     = Core10.IMAGE_ASPECT_COLOR_BIT
          , mipLevel       = level
          , baseArrayLayer = 0
          , layerCount     = 1
          }
        srcOffset = Core10.Offset3D (mipWidth predLevel) (mipHeight predLevel) 1
        dstOffset = Core10.Offset3D (mipWidth level) (mipHeight level) 1

    mipScale level x = max 1 . truncate @Double $ (0.5 ^ level) * fromIntegral x
    mipWidth  level = mipScale level width
    mipHeight level = mipScale level height
