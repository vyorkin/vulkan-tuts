{-# LANGUAGE OverloadedLists #-}

module Action where

import Control.Monad.IO.Class (MonadIO(..))
import Control.Monad.Managed (Managed, managed)
import Data.Foldable (for_)
import Data.List (genericLength)
import Data.Word (Word32)
import Vulkan.NamedType ((:::))

import qualified Linear
import qualified Vulkan.Core10 as Core10
import qualified Foreign

import Types

import qualified Buffer
import qualified Scene
import qualified Obj

setup :: VulkanWindow -> Managed SetupResources
setup vw = do
  -- obj <- liftIO $ Obj.loadObj "resources/cube.obj" -- "viking_room.obj"
  obj <- liftIO $ Obj.loadObj "resources/viking_room.obj"
  let Indexed objVs objIs = obj
  object <- (,,)
    <$> managed (Buffer.withVertices vw objVs)
    <*> managed (Buffer.withIndices vw objIs)
    <*> pure (genericLength objIs)

  compo <- (,)
    <$> managed (Buffer.withVertices vw Scene.compo)
    <*> genericLength Scene.compo

  let Indexed vertices indices = Scene.square2
  square <- (,,)
    <$> managed (Buffer.withVertices vw vertices)
    <*> managed (Buffer.withIndices vw indices)
    <*> pure (genericLength indices)

  pure
    ( compo
    , square
    , object
    )

type SetupResources =
  ( "compo"  ::: (Core10.Buffer, "vertexCount" ::: Word32)
  , "square" ::: (Core10.Buffer, Core10.Buffer, "indexCount" ::: Word32)
  , "obj"    ::: (Core10.Buffer, Core10.Buffer, "indexCound" ::: Word32)
  )

-- | This is a «static» scene layout, assembled with push constants and draw calls.
--   Basically a pipeline @main@ function to put out the frame image.
--   Check "MainLoop.drawFrame" for external inputs with UBOs.
commands
  :: SetupResources
  -> Core10.PipelineLayout
  -> Core10.CommandBuffer
  -> IO ()
commands setupResources pipelineLayout buffer = do
  -- pushConstants Linear.identity
    -- Scene.translate $ Linear.V3 1 0 (-1.0)

  -- -- Draw composition subscene at origin
  -- Core10.cmdBindVertexBuffers buffer 0 [compoVertex] [0]
  -- Core10.cmdDraw buffer compoSize 1 0 0

  -- -- Load a square to stamp at different positions
  -- Core10.cmdBindVertexBuffers buffer 0 [squareVertex] [0]
  -- Core10.cmdBindIndexBuffer buffer squareIndex 0 Core10.INDEX_TYPE_UINT16

  -- let
  --   models =
  --     [ Scene.translate $ Linear.V3    1 0 0.5
  --     , Scene.translate $ Linear.V3 (-1) 0 0.5
  --     , Scene.translate $ Linear.V3 (-1) 0 1.0
  --     , Scene.translate $ Linear.V3    1 0 1.0
  --     ] :: [Mat4]
  -- for_ models $ \model -> do
  --   pushConstants model

  Core10.cmdBindVertexBuffers buffer 0 [objectVertex] [0]
  Core10.cmdBindIndexBuffer buffer objectIndex 0 Core10.INDEX_TYPE_UINT16

  pushConstants Linear.identity
  Core10.cmdDrawIndexed buffer objectSize 1 0 0 0

  where
    (_compo, _square, object) = setupResources

    (objectVertex, objectIndex, objectSize) = object

    -- (compoVertex, compoSize) = compo
    -- (squareVertex, squareIndex, squareSize) = square

    pushConstants :: Mat4 -> IO ()
    pushConstants model =
      Foreign.with model $ \ptr ->
        Core10.cmdPushConstants
          buffer
          pipelineLayout
          Core10.SHADER_STAGE_VERTEX_BIT
          0
          sizeMat4
          (Foreign.castPtr ptr)
