module Image where

import Data.Word (Word32)
import Vulkan.Zero (zero)
import Vulkan.NamedType ((:::))
import Linear (V2(..))

import qualified Vulkan.Core10 as Core10

import Types (Allocate)

withImageView
  :: Allocate m
  -> Core10.Device
  -> Core10.Format
  -> Core10.ImageAspectFlagBits
  -> Core10.ComponentMapping
  -> "mipLevels" ::: Word32
  -> Core10.Image
  -> m Core10.ImageView
withImageView allocate device format aspectMask comps mipLevels image =
  Core10.withImageView device imageViewCI Nothing allocate
  where
    imageViewCI = zero
      { Core10.image            = image
      , Core10.viewType         = Core10.IMAGE_VIEW_TYPE_2D
      , Core10.format           = format
      , Core10.components       = comps
      , Core10.subresourceRange = subresource aspectMask mipLevels
      }

swizzleABGR :: Core10.ComponentMapping
swizzleABGR = Core10.ComponentMapping
  { r = Core10.COMPONENT_SWIZZLE_A
  , g = Core10.COMPONENT_SWIZZLE_B
  , b = Core10.COMPONENT_SWIZZLE_G
  , a = Core10.COMPONENT_SWIZZLE_R
  }

withSampler :: Allocate m -> Core10.Device -> m Core10.Sampler
withSampler allocate device = Core10.withSampler device samplerCI Nothing allocate
  where
    maxAnisotropy = 16

    samplerCI = zero
      { Core10.magFilter               = Core10.FILTER_LINEAR
      , Core10.minFilter               = Core10.FILTER_LINEAR
      , Core10.addressModeU            = Core10.SAMPLER_ADDRESS_MODE_REPEAT
      , Core10.addressModeV            = Core10.SAMPLER_ADDRESS_MODE_REPEAT
      , Core10.addressModeW            = Core10.SAMPLER_ADDRESS_MODE_REPEAT
      , Core10.anisotropyEnable        = maxAnisotropy > 1
      , Core10.maxAnisotropy           = maxAnisotropy
      , Core10.borderColor             = Core10.BORDER_COLOR_INT_OPAQUE_BLACK
      , Core10.unnormalizedCoordinates = False
      , Core10.compareEnable           = False
      , Core10.compareOp               = Core10.COMPARE_OP_ALWAYS
      , Core10.mipmapMode              = Core10.SAMPLER_MIPMAP_MODE_LINEAR
      , Core10.mipLodBias              = 0
      , Core10.minLod                  = 0
      , Core10.maxLod                  = Core10.LOD_CLAMP_NONE
      }

subresource
  :: Core10.ImageAspectFlags
  -> "mipLevels" ::: Word32
  -> Core10.ImageSubresourceRange
subresource aspectMask mipLevels = Core10.ImageSubresourceRange
  { aspectMask     = aspectMask
  , baseMipLevel   = 0
  , levelCount     = mipLevels -- XXX: including base
  , baseArrayLayer = 0
  , layerCount     = 1
  }

class MipLevels a where
  -- | Base level + a number of halvings before it turns into a single pixel.
  log2MipLevels :: Integral b => a -> b

instance Integral a => MipLevels (a, a) where
  log2MipLevels (w, h) = 1 + truncate @Double (logBase 2 . fromIntegral $ max w h)
  {-# INLINE log2MipLevels #-}

instance Integral a => MipLevels (V2 a) where
  log2MipLevels (V2 w h) = log2MipLevels (w, h)

instance MipLevels Core10.Extent2D where
  log2MipLevels (Core10.Extent2D w h) = log2MipLevels (w, h)

instance MipLevels Core10.Extent3D where
  log2MipLevels (Core10.Extent3D w h _d) = log2MipLevels (w, h)
