{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE QuasiQuotes #-}

module Shaders where

import Control.Exception (bracket)
import Control.Monad.Managed (Managed, managed)
import Data.Vector (Vector)
import Vulkan.Core10 as Core10
import Vulkan.CStruct.Extends (SomeStruct(..))
import Vulkan.Zero -- (zero)

import Vulkan.Utils.ShaderQQ (frag, vert)

create :: Core10.Device -> Managed (Vector (SomeStruct Core10.PipelineShaderStageCreateInfo))
create dev = do
  vertModule <- managed $ withShaderModule dev zero { code = vertCode } Nothing bracket
  let
    vertShaderStageCreateInfo = SomeStruct zero
      { stage   = SHADER_STAGE_VERTEX_BIT
      , module' = vertModule
      , name    = "main"
      }

  fragModule <- managed $ withShaderModule dev zero { code = fragCode } Nothing bracket
  let
    fragShaderStageCreateInfo = SomeStruct zero
      { stage   = SHADER_STAGE_FRAGMENT_BIT
      , module' = fragModule
      , name    = "main"
      }

  pure
    [ vertShaderStageCreateInfo
    , fragShaderStageCreateInfo
    ]
  where
    vertCode =
      [vert|
        #version 450
        #extension GL_ARB_separate_shader_objects : enable

        layout(push_constant) uniform MAX_SIZE_128_CRY {
          mat4 model;
        } obj;

        layout(set=0, binding=0) uniform PER_FRAME {
          mat4 model;
          mat4 view;
          mat4 projection;
        } scene;

        layout(location = 0) in vec3 inPosition;
        layout(location = 1) in vec3 inColor;
        layout(location = 2) in vec2 inTexCoord;

        layout(location = 0) out vec3 fragColor;
        layout(location = 1) out vec2 fragTexCoord;

        void main() {
          gl_Position = scene.projection * scene.view * scene.model * obj.model * vec4(inPosition, 1.0);
          // gl_Position = obj.model * vec4(inPosition, 1.0);
          fragColor = inColor;
          fragTexCoord = inTexCoord;
        }
      |]

    fragCode =
      [frag|
        #version 450
        #extension GL_ARB_separate_shader_objects : enable

        layout(binding = 1) uniform sampler2D texSampler;

        layout(location = 0) in vec3 fragColor;
        layout(location = 1) in vec2 fragTexCoord;

        layout(location = 0) out vec4 outColor;

        void main() {
          vec4 texel = texture(texSampler, fragTexCoord);

          // outColor = vec4(fragColor, 1.0);
          outColor = vec4(pow(texel.rgb, vec3(fragColor)), texel.a);
          // outColor = vec4(fragTexCoord, 0.0, 1.0);
        }
      |]
