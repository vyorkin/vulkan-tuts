module Types where

import Control.Exception (bracket)
import Control.Monad.Managed (Managed, managed)
import Data.Bits (Bits, zeroBits, (.&.))
import Data.Hashable (Hashable(..))
import Data.List.NonEmpty (NonEmpty(..))
import Data.Vector (Vector)
import Data.Word (Word16, Word32)
import GHC.Generics (Generic)
import Linear (V2(..), V3(..), M44)
import Vulkan.Core10 as Core10
import Vulkan.Extensions.VK_KHR_swapchain -- (SwapchainKHR(..), swapchains, imageIndices)
import Vulkan.Zero (Zero(..))

import qualified Foreign
import qualified VulkanMemoryAllocator as VMA
import qualified SDL
import qualified Data.HashMap.Strict as HashMap

type Allocate m = forall a . IO a -> (a -> IO ()) -> m a

----------------------------------------------------------------
-- Resource handling with 'managed'
----------------------------------------------------------------

allocate :: Allocate Managed
allocate c d = managed (bracket c d)

data UBO a = UBO
  { uboBuffer     :: Buffer
  , uboAllocation :: VMA.Allocation
  } deriving (Show)

data TextureImage = TextureImage
  { tiImage     :: Core10.Image
  , tiImageView :: Core10.ImageView
  , tiSize      :: V2 Float
  , tiMipLevels :: Word32
  } deriving (Show)

type SyncObjects = NonEmpty SyncObject

-- XXX: the tutorial uses additional mutable vector for "current" fence
--      IDK how to use that since objects loop is picking them automatically.
data SyncObject = SyncObject
  { soIndex          :: Int
  , soRenderFinished :: Semaphore
  , soImageAvailable :: Semaphore
  , soInFlightFence  :: Fence
  }

data VulkanWindow = VulkanWindow
  { vwSdlWindow                :: SDL.Window
  , vwDevice                   :: Device
  , vwCommandPool              :: CommandPool
  , vwAllocator                :: VMA.Allocator
  , vwSurface                  :: SurfaceKHR
  , vwSwapchain                :: SwapchainKHR
  , vwExtent                   :: Extent2D
  , vwFormat                   :: Format
  , vwDepthFormat              :: Format
  , vwMsaaSamples              :: Core10.SampleCountFlagBits
  , vwImageViews               :: Vector ImageView
  , vwGraphicsQueue            :: Queue
  , vwGraphicsQueueFamilyIndex :: Word32
  , vwPresentQueue             :: Queue
  , vwDebug                    :: String -> IO ()
  }

data Vertex = Vertex
  { vPos      :: V3 Float
  , vColor    :: V3 Float
  , vTexCoord :: V2 Float
  }
  deriving (Eq, Ord, Show, Generic)

instance Hashable Vertex

vertexItems :: Num b => [Vertex] -> ([Float], b)
vertexItems vertices = (items, size)
  where
    size = fromIntegral $
      Foreign.sizeOf (head items) * length items

    items = do
      Vertex{..} <- vertices
      let V3 x y z = vPos
      let V3 r g b = vColor
      let V2 u v = vTexCoord
      [ x, y, z, r, g, b, u, v ]

data Indexed a = Indexed
  { iItems   :: [a]
  , iIndices :: [Word16]
  }
  deriving (Show)

indexed :: (Eq a, Hashable a, Foldable t) => t a -> Indexed a
indexed xs = Indexed
  { iItems   = reverse unique -- XXX: indices are counted from the right-most element.
  , iIndices = indices        -- XXX: using left fold would only @add@ a 'reverse' here.
  }
  where
    (_seen, unique, indices) = foldr go (mempty, mempty, mempty) xs

    go item (seen, itemAcc, indexAcc) =
      case HashMap.lookup item seen of
        Just oldIx ->
          (seen, itemAcc, oldIx : indexAcc)
        Nothing ->
          let
            newIx = fromIntegral $ HashMap.size seen
          in
            ( HashMap.insert item newIx seen
            , item : itemAcc
            , newIx : indexAcc
            )

data MVP = MVP
  { mvpModel      :: M44 Float
  , mvpView       :: M44 Float
  , mvpProjection :: M44 Float
  }
  deriving (Show)

instance Foreign.Storable MVP where
  {-# INLINE sizeOf #-}
  sizeOf _mvp =
    Foreign.sizeOf (0 :: V3 (M44 Float))
    -- Foreign.sizeOf mvpModel +
    -- Foreign.sizeOf mvpView +
    -- Foreign.sizeOf mvpProjection

  {-# INLINE alignment #-}
  alignment _mvp =
    Foreign.alignment (0 :: V3 (M44 Float))

  {-# INLINE poke #-}
  poke ptr MVP{..} =
    Foreign.poke (Foreign.castPtr ptr) (V3 mvpModel mvpView mvpProjection)
  -- poke ptr MVP{..} = do
  --   Foreign.poke ptr' mvpModel
  --   Foreign.pokeElemOff ptr' 1 mvpView
  --   Foreign.pokeElemOff ptr' 2 mvpProjection
  --   where
  --     ptr' = Foreign.castPtr ptr

  {-# INLINE peek #-}
  peek ptr = do
    V3 m v p <- Foreign.peek (Foreign.castPtr ptr)
    pure $ MVP m v p
    -- MVP
    -- <$> Foreign.peek ptr'
    -- <*> Foreign.peekElemOff ptr' 1
    -- <*> Foreign.peekElemOff ptr' 2
    -- where
    --   ptr' = Foreign.castPtr ptr

-- XXX: To use with 'sizeOf' instead of 'undefined'.
instance Zero MVP where
  zero = MVP 0 0 0

type Mat4 = M44 Float

sizeMat4 :: Word32
sizeMat4 = fromIntegral $ Foreign.sizeOf (0 :: M44 Float)

----------------------------------------------------------------
-- Bit utils
----------------------------------------------------------------

(.&&.) :: Bits a => a -> a -> Bool
x .&&. y = (/= zeroBits) (x .&. y)
