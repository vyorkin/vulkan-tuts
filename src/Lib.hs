{-# LANGUAGE OverloadedLists #-}

module Lib where

import Control.Exception (bracket, bracket_)
import Control.Monad (guard)
import Control.Monad.IO.Class (MonadIO(..))
import Control.Monad.Managed (Managed, managed)
import Control.Monad.Trans.Maybe (MaybeT(..), runMaybeT)
import Data.Bits ((.|.), (.&.))
import Data.Foldable (for_)
import Data.List (nub)
import Data.Ord (comparing)
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8, encodeUtf8)
import Data.Traversable (for)
import Data.Vector (Vector)
import Data.Word (Word32, Word64)
import Foreign.Ptr (castPtr)
import Say (sayErr)
import SDL.Video.Vulkan (vkLoadLibrary, vkCreateSurface, vkUnloadLibrary, vkGetInstanceExtensions)
import System.Exit (exitFailure)
import Vulkan.Core10 as Core10
import Vulkan.CStruct.Extends (SomeStruct(..))
import Vulkan.Extensions.VK_EXT_debug_utils as Ext
import Vulkan.Extensions.VK_KHR_surface as Khr
import Vulkan.Extensions.VK_KHR_swapchain as Khr
import Vulkan.NamedType ((:::))
import Vulkan.Version (pattern MAKE_VERSION)
import Vulkan.Zero (zero)

import qualified Data.ByteString as BS
import qualified Data.Text as Text
import qualified Data.Vector as Vector
import qualified Foreign
import qualified Linear
import qualified SDL
import qualified Vulkan.Core11 as Core11
import qualified VulkanMemoryAllocator as VMA

import Types

import qualified Buffer
import qualified Buffer.Image
import qualified Depth
import qualified Image

createSyncObject :: VulkanWindow -> Int -> Managed SyncObject
createSyncObject vw ix = SyncObject
  <$> pure ix
  <*> Lib.createSemaphore vw
  <*> Lib.createSemaphore vw
  <*> Lib.createFence vw

createSemaphore :: VulkanWindow -> Managed Semaphore
createSemaphore VulkanWindow{vwDevice} =
  Core10.withSemaphore vwDevice zero Nothing allocate

createFence :: VulkanWindow -> Managed Fence
createFence VulkanWindow{vwDevice} =
  Core10.withFence vwDevice fenceCreateInfo Nothing allocate
  where
    fenceCreateInfo :: FenceCreateInfo '[]
    fenceCreateInfo = zero
      { flags = FENCE_CREATE_SIGNALED_BIT
      }

type PipelineInfo = (Pipeline, PipelineLayout, Vector DescriptorSet)

createCommandBuffers
  :: VulkanWindow
  -> RenderPass
  -> PipelineInfo
  -> Vector Framebuffer
  -> (VulkanWindow -> Managed t)
  -> (t -> PipelineLayout -> CommandBuffer -> IO ())
  -> Managed (Vector CommandBuffer)
createCommandBuffers vw@VulkanWindow{..} renderPass pipelineInfo framebuffers actionSetup actionCommands = do
  let
    commandBufferAllocateInfo :: CommandBufferAllocateInfo
    commandBufferAllocateInfo = zero
      { commandPool        = vwCommandPool
      , level              = COMMAND_BUFFER_LEVEL_PRIMARY
      , commandBufferCount = fromIntegral $ Vector.length framebuffers
      }
  buffers <- withCommandBuffers vwDevice commandBufferAllocateInfo allocate

  setupResources <- actionSetup vw

  let
    beginInfo :: CommandBufferBeginInfo '[]
    beginInfo = zero { flags = COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT }
  liftIO . for_ (Vector.zip3 framebuffers buffers descSets) $ \(framebuffer, buffer, descSet) ->
    useCommandBuffer buffer beginInfo $ do
      let
        clearColor = Color (Float32 (0.025, 0.0125, 0.025, 0))
        renderPassBeginInfo = zero
          { renderPass  = renderPass
          , framebuffer = framebuffer
          , renderArea  = Rect2D
              { offset = zero
              , extent = vwExtent
              }
          , clearValues =
              [ clearColor
              , DepthStencil (ClearDepthStencilValue 1.0 0)
              , clearColor
              ]
          }

      Core10.cmdUseRenderPass buffer renderPassBeginInfo SUBPASS_CONTENTS_INLINE $ do
        Core10.cmdBindPipeline
          buffer
          PIPELINE_BIND_POINT_GRAPHICS
          graphicsPipeline

        Core10.cmdBindDescriptorSets
          buffer
          PIPELINE_BIND_POINT_GRAPHICS
          pipelineLayout
          0
          [descSet]
          []

        actionCommands setupResources pipelineLayout buffer

  pure buffers
  where
    (graphicsPipeline, pipelineLayout, descSets) = pipelineInfo

createRenderPass :: VulkanWindow -> Managed RenderPass
createRenderPass VulkanWindow{vwDevice, vwFormat, vwDepthFormat, vwMsaaSamples} = do
  withRenderPass vwDevice
    zero
      { attachments  = [colorAttachment, depthAttachment, colorAttachmentResolve]
      , subpasses    = [subpass]
      , dependencies = [subpassDependency]
      }
    Nothing
    allocate
  where
    colorAttachment :: Core10.AttachmentDescription
    colorAttachment = zero
      { format         = vwFormat
      , samples        = vwMsaaSamples
      , loadOp         = ATTACHMENT_LOAD_OP_CLEAR
      , storeOp        = ATTACHMENT_STORE_OP_STORE
      , stencilLoadOp  = ATTACHMENT_LOAD_OP_DONT_CARE
      , stencilStoreOp = ATTACHMENT_STORE_OP_DONT_CARE
      , initialLayout  = IMAGE_LAYOUT_UNDEFINED
      , finalLayout    = IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
      }

    depthAttachment :: Core10.AttachmentDescription
    depthAttachment = zero
      { format         = vwDepthFormat
      , samples        = vwMsaaSamples
      , loadOp         = ATTACHMENT_LOAD_OP_CLEAR
      , storeOp        = ATTACHMENT_STORE_OP_DONT_CARE
      , stencilLoadOp  = ATTACHMENT_LOAD_OP_DONT_CARE
      , stencilStoreOp = ATTACHMENT_STORE_OP_DONT_CARE
      , initialLayout  = IMAGE_LAYOUT_UNDEFINED
      , finalLayout    = IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
      }

    colorAttachmentResolve :: Core10.AttachmentDescription
    colorAttachmentResolve = zero
      { format         = vwFormat
      , samples        = SAMPLE_COUNT_1_BIT
      , loadOp         = ATTACHMENT_LOAD_OP_CLEAR
      , storeOp        = ATTACHMENT_STORE_OP_STORE
      , stencilLoadOp  = ATTACHMENT_LOAD_OP_DONT_CARE
      , stencilStoreOp = ATTACHMENT_STORE_OP_DONT_CARE
      , initialLayout  = IMAGE_LAYOUT_UNDEFINED
      , finalLayout    = IMAGE_LAYOUT_PRESENT_SRC_KHR
      }

    subpass :: SubpassDescription
    subpass = zero
      { pipelineBindPoint = PIPELINE_BIND_POINT_GRAPHICS
      , colorAttachments =
          [ zero
              { attachment = 0
              , layout     = IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
              }
          ]
      , depthStencilAttachment = Just zero
          { attachment = 1
          , layout     = IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
          }
      , resolveAttachments =
          [ zero
              { attachment = 2
              , layout     = IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
              }
          ]
      }

    subpassDependency :: SubpassDependency
    subpassDependency = zero
      { srcSubpass    = SUBPASS_EXTERNAL
      , dstSubpass    = 0
      , srcStageMask  = PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
      , srcAccessMask = zero
      , dstStageMask  = PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
      , dstAccessMask = ACCESS_COLOR_ATTACHMENT_READ_BIT
                          .|. ACCESS_COLOR_ATTACHMENT_WRITE_BIT
      }

type CreateShaders = Device -> Managed (Vector (SomeStruct PipelineShaderStageCreateInfo))

createGraphicsPipeline
  :: VulkanWindow
  -> RenderPass
  -> CreateShaders
  -> Managed (PipelineInfo, Vector (UBO (Linear.V3 Mat4)))
createGraphicsPipeline vw@VulkanWindow{vwDevice, vwImageViews, vwExtent} renderPass createShaders = do
  shaderStages <- createShaders vwDevice

  let imageViews = Vector.length vwImageViews
  liftIO . putStrLn $ "imageViews: " <> show imageViews

  let
    descriptorSetInfo :: Core10.DescriptorSetLayoutCreateInfo '[]
    descriptorSetInfo = zero
      { bindings =
          [ Core10.DescriptorSetLayoutBinding
              { binding           = 0
              , descriptorType    = Core10.DESCRIPTOR_TYPE_UNIFORM_BUFFER
              , stageFlags        = SHADER_STAGE_VERTEX_BIT
              , descriptorCount   = 1
              , immutableSamplers = mempty
              }
          , Core10.DescriptorSetLayoutBinding
              { binding           = 1
              , stageFlags        = SHADER_STAGE_FRAGMENT_BIT
              , descriptorType    = Core10.DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER
              , descriptorCount   = 1
              , immutableSamplers = mempty
              }
          ]
      }
  descLayout <- Core10.withDescriptorSetLayout vwDevice descriptorSetInfo Nothing allocate

  let
    layoutInfo = Core10.PipelineLayoutCreateInfo
      { flags = zero -- "reserved for future use."
      , setLayouts = [descLayout]
      , pushConstantRanges =
          [ PushConstantRange -- Projection + View * Model
              { stageFlags = Core10.SHADER_STAGE_VERTEX_BIT
              , size       = sizeMat4
              , offset     = 0
              }
          ]
      }
  pipelineLayout <- withPipelineLayout vwDevice layoutInfo Nothing allocate

  let
    descriptorPoolCreateInfo :: Core10.DescriptorPoolCreateInfo '[]
    descriptorPoolCreateInfo = zero
      { Core10.maxSets = fromIntegral imageViews
      , Core10.poolSizes =
          [ Core10.DescriptorPoolSize
              Core10.DESCRIPTOR_TYPE_UNIFORM_BUFFER
              (fromIntegral imageViews)
          , Core10.DescriptorPoolSize
              Core10.DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER
              (fromIntegral imageViews)
          ]
      }
  descPool <- Core10.withDescriptorPool vwDevice descriptorPoolCreateInfo Nothing allocate

  let
    descriptorSetAllocateInfo :: Core10.DescriptorSetAllocateInfo '[]
    descriptorSetAllocateInfo = zero
      { Core10.descriptorPool = descPool
      , Core10.setLayouts     = Vector.replicate imageViews descLayout
      }

  (textureX, textureMipLevels, _alloc) <- managed $ bracket
    (Buffer.Image.createImage vw "resources/viking_room.png")
    (Buffer.Image.destroyImage vw)

  textureView <- Image.withImageView
    allocate
    vwDevice
    Core10.FORMAT_R8G8B8A8_SRGB
    Core10.IMAGE_ASPECT_COLOR_BIT
    Image.swizzleABGR
    textureMipLevels
    textureX
  textureSampler <- Image.withSampler allocate vwDevice

  descSets <- Core10.allocateDescriptorSets vwDevice descriptorSetAllocateInfo
  ubos <- for descSets $ \descSet -> do
    scene <- managed $ Buffer.withUniform vw $
      Linear.V3 Linear.identity Linear.identity Linear.identity

    let
      sceneBufferInfo = Core10.DescriptorBufferInfo
        { buffer = uboBuffer scene
        , offset = 0
        , range  = Core10.WHOLE_SIZE
        }

      writeScene = SomeStruct zero
        { dstSet          = descSet
        , dstBinding      = 0
        , dstArrayElement = 0
        , descriptorType  = Core10.DESCRIPTOR_TYPE_UNIFORM_BUFFER
        , descriptorCount = 1
        , bufferInfo      = [sceneBufferInfo]
        }

    let
      textureImageInfo = Core10.DescriptorImageInfo
        { sampler     = textureSampler
        , imageView   = textureView
        , imageLayout = Core10.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        }

      writeTexture = SomeStruct zero
        { dstSet = descSet
        , dstBinding      = 1
        , dstArrayElement = 0
        , descriptorType  = Core10.DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER
        , descriptorCount = 1
        , imageInfo       = [textureImageInfo]
        }

    liftIO . putStrLn $ "updateDescriptorSets: " <> show writeScene
    Core10.updateDescriptorSets vwDevice [writeScene, writeTexture] mempty

    pure scene

  liftIO . putStrLn $ "perFrame: " <> show (descSets, ubos)

  let
    pipelineCreateInfo = SomeStruct zero
      { stages             = shaderStages
      , vertexInputState   = Just $ SomeStruct zero
          { vertexBindingDescriptions =
              [ VertexInputBindingDescription
                  { binding   = 0
                  , stride    = fromIntegral $ Foreign.sizeOf @Float 0 * (3+3+2)
                  , inputRate = VERTEX_INPUT_RATE_VERTEX
                  }
              ]
          , vertexAttributeDescriptions =
              [ VertexInputAttributeDescription
                  { location = 0
                  , binding  = 0
                  , format   = FORMAT_R32G32B32_SFLOAT
                  , offset   = fromIntegral $ Foreign.sizeOf @Float 0 * (0)
                  }
              , VertexInputAttributeDescription
                  { location = 1
                  , binding  = 0
                  , format   = FORMAT_R32G32B32_SFLOAT
                  , offset   = fromIntegral $ Foreign.sizeOf @Float 0 * (0+3)
                  }
              , VertexInputAttributeDescription
                  { location = 2
                  , binding  = 0
                  , format   = FORMAT_R32G32_SFLOAT
                  , offset   = fromIntegral $ Foreign.sizeOf @Float 0 * (0+3+3)
                  }
              ]
          }
      , inputAssemblyState = Just zero
          { topology               = PRIMITIVE_TOPOLOGY_TRIANGLE_LIST
          , primitiveRestartEnable = False
          }
      , viewportState = Just $ SomeStruct zero
          { viewports =
            [ Viewport
                { x        = 0
                , y        = 0
                , width    = realToFrac (width (vwExtent :: Extent2D))
                , height   = realToFrac (height (vwExtent :: Extent2D))
                , minDepth = 0
                , maxDepth = 1
                }
            ]
          , scissors =
              [ Rect2D
                  { offset = Offset2D 0 0
                  , extent = vwExtent
                  }
              ]
          }
      , rasterizationState = SomeStruct zero
          { depthClampEnable        = False
          , rasterizerDiscardEnable = False
          , lineWidth               = 1
          , polygonMode             = POLYGON_MODE_FILL
          , cullMode                = CULL_MODE_BACK_BIT
          , frontFace               = FRONT_FACE_CLOCKWISE
          -- XXX: tutorial says so. But then, they use GLM and invert everything.
          -- , frontFace               = FRONT_FACE_COUNTER_CLOCKWISE
          , depthBiasEnable         = False
          }
      , multisampleState = Just $ SomeStruct zero
          { rasterizationSamples = vwMsaaSamples vw
          , sampleShadingEnable  = True -- False
          , minSampleShading     = 0.2 -- 1
          , sampleMask           = [maxBound]
          }
      , depthStencilState = Just zero
          { depthTestEnable       = True
          , depthWriteEnable      = True
          , depthCompareOp        = Core10.COMPARE_OP_LESS
          , depthBoundsTestEnable = False
          , minDepthBounds        = 0.0 -- Optional
          , maxDepthBounds        = 1.0 -- Optional
          , stencilTestEnable     = False
          , front                 = zero -- Optional
          , back                  = zero -- Optional
          }
      , colorBlendState = Just $ SomeStruct zero
          { logicOpEnable = False
          , attachments =
              [ zero
                  { colorWriteMask =
                      COLOR_COMPONENT_R_BIT .|.
                      COLOR_COMPONENT_G_BIT .|.
                      COLOR_COMPONENT_B_BIT .|.
                      COLOR_COMPONENT_A_BIT
                  , blendEnable = False
                  }
              ]
          }
      , dynamicState       = Nothing -- Just dynamicState'
      , layout             = pipelineLayout
      , renderPass         = renderPass
      , subpass            = 0
      , basePipelineHandle = zero
      }

  withGraphicsPipelines vwDevice zero [pipelineCreateInfo] Nothing allocate >>= \case
    (SUCCESS, [pipeline]) ->
      pure
        ( ( pipeline
          , pipelineLayout
          , descSets
          )
        , ubos
        )
    (err, pipes) ->
      error $ "Pipelines allocation failed? " <> show (err, pipes)

  where
    _dynamicState' = PipelineDynamicStateCreateInfo
      { flags = zero
      , dynamicStates =
          [ DYNAMIC_STATE_VIEWPORT
          ]
      }

createFramebuffers :: VulkanWindow -> RenderPass -> Managed (Vector Framebuffer)
createFramebuffers vw@VulkanWindow{vwDevice, vwExtent, vwImageViews} renderPass = do
  colorView <- createColorResource vw
  depthView <- Depth.createDepthTextureImage vw

  for vwImageViews $ \colorResolve -> do
    let framebufferCreateInfo :: FramebufferCreateInfo '[]
        framebufferCreateInfo = zero
          { renderPass  = renderPass
          , attachments = [colorView, depthView, colorResolve]
          , width       = width (vwExtent :: Extent2D)
          , height      = height (vwExtent :: Extent2D)
          , layers      = 1
          }
    withFramebuffer vwDevice framebufferCreateInfo Nothing allocate

createColorResource :: VulkanWindow -> Managed ImageView
createColorResource vw = do
  (image, _allocation, _info) <- VMA.withImage (vwAllocator vw) imageCI imageAllocationCI allocate
  Core10.withImageView (vwDevice vw) (imageViewCI image) Nothing allocate
  where
    imageCI = zero
      { imageType     = IMAGE_TYPE_2D
      , format        = vwFormat vw
      , extent        = Core10.Extent3D width height 1
      , mipLevels     = 1
      , arrayLayers   = 1
      , tiling        = Core10.IMAGE_TILING_OPTIMAL
      , initialLayout = Core10.IMAGE_LAYOUT_UNDEFINED
      , usage         = Core10.IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT .|. Core10.IMAGE_USAGE_COLOR_ATTACHMENT_BIT
      , sharingMode   = Core10.SHARING_MODE_EXCLUSIVE
      , samples       = vwMsaaSamples vw
      }
      where
        Core10.Extent2D width height = vwExtent vw

    imageAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_CPU_TO_GPU
      , VMA.requiredFlags = Core10.MEMORY_PROPERTY_DEVICE_LOCAL_BIT
      }

    imageViewCI image = zero
      { Core10.image            = image
      , Core10.viewType         = Core10.IMAGE_VIEW_TYPE_2D
      , Core10.format           = vwFormat vw
      , Core10.components       = zero
      , Core10.subresourceRange = Image.subresource IMAGE_ASPECT_COLOR_BIT 1
      }


withVulkanWindow :: Text -> Int -> Int -> [Text] -> Managed VulkanWindow
withVulkanWindow windowTitle width height layers = do
  window             <- managed $ withWindow windowTitle width height
  instanceCreateInfo <- windowInstanceCreateInfo window windowTitle layers
  inst               <- withInstance instanceCreateInfo Nothing allocate
  _debugExt          <- withDebugUtilsMessengerEXT inst debugUtilsMessengerCreateInfo Nothing allocate

  surface <- managed $ withSDLWindowSurface inst window

  (dev, allocator, graphicsQueue, graphicsQueueFamilyIndex, presentQueue, depthFormat, swapchainFormat, swapchainExtent, swapchain, msaaSamples) <-
    createGraphicalDevice inst surface (Extent2D (fromIntegral width) (fromIntegral height))

  let
    commandPoolCreateInfo :: CommandPoolCreateInfo
    commandPoolCreateInfo = zero
      { queueFamilyIndex = graphicsQueueFamilyIndex
      }
  commandPool <- withCommandPool dev commandPoolCreateInfo Nothing allocate

  (_res, images) <- getSwapchainImagesKHR dev swapchain
  imageViews <- for images $
    Image.withImageView allocate dev swapchainFormat Core10.IMAGE_ASPECT_COLOR_BIT zero 1

  let
    debugMessage msg =
      submitDebugUtilsMessageEXT
        inst
        DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT
        DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
        zero { message = encodeUtf8 (Text.pack msg) }
  liftIO $ debugMessage "Test"

  pure VulkanWindow
    { vwSdlWindow                = window
    , vwDevice                   = dev
    , vwCommandPool              = commandPool
    , vwAllocator                = allocator
    , vwSurface                  = surface
    , vwSwapchain                = swapchain
    , vwExtent                   = swapchainExtent
    , vwFormat                   = swapchainFormat
    , vwDepthFormat              = depthFormat
    , vwMsaaSamples              = msaaSamples
    , vwImageViews               = imageViews
    , vwGraphicsQueue            = graphicsQueue
    , vwGraphicsQueueFamilyIndex = graphicsQueueFamilyIndex
    , vwPresentQueue             = presentQueue
    , vwDebug                    = debugMessage
    }

-- | InstanceCreateInfo for an SDL window
windowInstanceCreateInfo
  :: MonadIO m
  => SDL.Window
  -> Text
  -> [Text]
  -> m (InstanceCreateInfo '[DebugUtilsMessengerCreateInfoEXT])
windowInstanceCreateInfo window title layers = do
  windowExtensions <- liftIO $
    traverse BS.packCString =<< vkGetInstanceExtensions window

  pure zero
    { next =
        ( debugUtilsMessengerCreateInfo
        , ()
        )
    , applicationInfo = Just zero
        { applicationName = Just (encodeUtf8 title)
        , apiVersion = MAKE_VERSION 1 1 0
        }
    , enabledLayerNames =
        Vector.fromList $ map encodeUtf8 layers
    , enabledExtensionNames =
        Vector.fromList $
          EXT_DEBUG_UTILS_EXTENSION_NAME :
          windowExtensions
    }

createGraphicalDevice
  :: Instance
  -> SurfaceKHR
  -> Extent2D
  -> Managed
      ( Device
      , VMA.Allocator
      , Queue
      , Word32
      , Queue
      , ("depth" ::: Format)
      , ("swapchain" ::: Format)
      , Extent2D
      , SwapchainKHR
      , Core10.SampleCountFlagBits
      )
createGraphicalDevice inst surface windowExtent = do
  let
    requiredDeviceExtensions =
      [ KHR_SWAPCHAIN_EXTENSION_NAME
      ]

  (physicalDevice, graphicsQueueFamilyIndex, presentQueueFamilyIndex, surfaceFormat, presentMode, surfaceCaps) <-
    pickGraphicalPhysicalDevice
      inst
      surface
      requiredDeviceExtensions
      (SurfaceFormatKHR FORMAT_B8G8R8_UNORM COLOR_SPACE_SRGB_NONLINEAR_KHR)

  Core10.PhysicalDeviceProperties{deviceName, limits} <- getPhysicalDeviceProperties physicalDevice
  sayErr $ "Using device: " <> decodeUtf8 deviceName
  sayErr $ "Device limits: " <> Text.pack (show limits)
  let
    sampleCounts =
      framebufferColorSampleCounts limits .&.
      framebufferDepthSampleCounts limits

    sampleCandidates =
      [ Core10.SAMPLE_COUNT_16_BIT
      , Core10.SAMPLE_COUNT_8_BIT
      , Core10.SAMPLE_COUNT_4_BIT
      , Core10.SAMPLE_COUNT_2_BIT
      , Core10.SAMPLE_COUNT_1_BIT
      ]

    samplesAvailable = do
      countBit <- sampleCandidates
      guard $ sampleCounts .&&. countBit
      pure countBit

    msaaSamples = case samplesAvailable of
      [] ->
        error "assert: at least SAMPLE_COUNT_1 is available"
      first : _rest ->
        first

  let
    deviceCreateInfo :: DeviceCreateInfo '[]
    deviceCreateInfo = zero
      { queueCreateInfos = Vector.fromList $ do
          i <- nub [graphicsQueueFamilyIndex, presentQueueFamilyIndex]
          pure $ SomeStruct zero
            { queueFamilyIndex = i
            , queuePriorities = [1]
            , flags = DEVICE_QUEUE_CREATE_PROTECTED_BIT
            }
      , enabledExtensionNames =
          requiredDeviceExtensions
      , enabledFeatures = Just zero
          { sampleRateShading = True
          , samplerAnisotropy = True
          }
      }
  dev <- Core10.withDevice physicalDevice deviceCreateInfo Nothing allocate

  let
    -- > ... if you double-buffer your command buffers, so resources used for rendering
    -- > in previous frame may still be in use by the GPU at the moment you allocate
    -- > resources needed for the current frame, set this value to 1.
    -- > If you want to allow any allocations other than used in the current frame
    -- > to become lost, set this value to 0.
    frameInUseCount = 1

    allocatorInfo :: VMA.AllocatorCreateInfo
    allocatorInfo = zero
      { VMA.physicalDevice  = Core10.physicalDeviceHandle physicalDevice
      , VMA.device          = Core10.deviceHandle dev
      , VMA.frameInUseCount = frameInUseCount
      , VMA.instance'       = Core10.instanceHandle inst
      }
  allocator <- VMA.withAllocator allocatorInfo allocate

  graphicsQueue <- Core11.getDeviceQueue2
    dev
    zero { Core11.queueFamilyIndex = graphicsQueueFamilyIndex
         , Core11.flags            = DEVICE_QUEUE_CREATE_PROTECTED_BIT
         }
  presentQueue <- Core11.getDeviceQueue2
    dev
    zero { Core11.queueFamilyIndex = presentQueueFamilyIndex
         , Core11.flags            = DEVICE_QUEUE_CREATE_PROTECTED_BIT
         }
  let
    swapchainCreateInfo :: SwapchainCreateInfoKHR '[]
    swapchainCreateInfo =
      let
        (sharingMode, queueFamilyIndices) =
          if graphicsQueue == presentQueue then
            ( SHARING_MODE_EXCLUSIVE
            , []
            )
          else
            ( SHARING_MODE_CONCURRENT
            , [graphicsQueueFamilyIndex, presentQueueFamilyIndex]
            )
      in
        zero
          { surface            = surface
          , minImageCount      = succ $ minImageCount (surfaceCaps :: SurfaceCapabilitiesKHR)
          , imageFormat        = (format :: SurfaceFormatKHR -> Format) surfaceFormat
          , imageColorSpace    = colorSpace surfaceFormat
          , imageExtent        = case
                                   currentExtent
                                     (surfaceCaps :: SurfaceCapabilitiesKHR)
                                 of
                                   Extent2D w h | w == maxBound, h == maxBound ->
                                     windowExtent
                                   e -> e
          , imageArrayLayers   = 1
          , imageUsage         = IMAGE_USAGE_COLOR_ATTACHMENT_BIT
          , imageSharingMode   = sharingMode
          , queueFamilyIndices = queueFamilyIndices
          , preTransform       = currentTransform
                                   (surfaceCaps :: SurfaceCapabilitiesKHR)
          , compositeAlpha     = COMPOSITE_ALPHA_OPAQUE_BIT_KHR
          , presentMode        = presentMode
          , clipped            = True
          }
  swapchain <- withSwapchainKHR dev swapchainCreateInfo Nothing allocate
  depthFormat <- Depth.getOptimalDepthFormat physicalDevice

  pure
    ( dev
    , allocator
    , graphicsQueue
    , graphicsQueueFamilyIndex
    , presentQueue
    , depthFormat
    , format (surfaceFormat :: SurfaceFormatKHR)
    , imageExtent (swapchainCreateInfo :: SwapchainCreateInfoKHR '[])
    , swapchain
    , msaaSamples
    )

-- | Find the device which has the most memory and a graphics queue family index
pickGraphicalPhysicalDevice
  :: MonadIO m
  => Instance
  -> SurfaceKHR
  -> Vector BS.ByteString
  -> SurfaceFormatKHR
  -> m
       ( PhysicalDevice
       , Word32
       , Word32
       , SurfaceFormatKHR
       , PresentModeKHR
       , SurfaceCapabilitiesKHR
       )
pickGraphicalPhysicalDevice inst surface _requiredExtensions desiredFormat = do
  (_result, devs) <- enumeratePhysicalDevices inst
  -- All devices with support for all the graphical features we want
  graphicsDevs <- fmap (Vector.mapMaybe id) . for devs $ \dev -> runMaybeT $ do
    graphicsQueue <- MaybeT $ headMay <$> getGraphicsQueueIndices dev
    presentQueue  <- MaybeT $ headMay <$> getPresentQueueIndices dev
    guard =<< deviceHasSwapChain dev
    bestFormat  <- getFormat dev
    presentMode <- getPresentMode dev
    surfaceCaps <- getPhysicalDeviceSurfaceCapabilitiesKHR dev surface
    mem         <- getPhysicalDeviceMemoryProperties dev
    score       <- deviceScore dev mem
    pure
      ( score
      , (dev, graphicsQueue, presentQueue, bestFormat, presentMode, surfaceCaps)
      )
  if Vector.null graphicsDevs
    then do
      sayErr "No suitable devices found"
      liftIO exitFailure
    else
      pure . snd $ Vector.maximumBy (comparing fst) graphicsDevs

 where
  headMay = \case
    [] -> Nothing
    xs -> Just (Vector.unsafeHead xs)

  deviceScore :: MonadIO m => PhysicalDevice -> PhysicalDeviceMemoryProperties -> m Word64
  deviceScore _dev mem = do
    let heaps = memoryHeaps mem
    let totalSize = sum $ (size :: MemoryHeap -> DeviceSize) <$> heaps
    pure totalSize

  deviceHasSwapChain :: MonadIO m => PhysicalDevice -> m Bool
  deviceHasSwapChain dev = do
    (_, extensions) <- enumerateDeviceExtensionProperties dev Nothing
    pure $ Vector.any ((KHR_SWAPCHAIN_EXTENSION_NAME ==) . extensionName) extensions

  getGraphicsQueueIndices :: MonadIO m => PhysicalDevice -> m (Vector Word32)
  getGraphicsQueueIndices dev = do
    queueFamilyProperties <- getPhysicalDeviceQueueFamilyProperties dev
    let isGraphicsQueue q =
          (QUEUE_GRAPHICS_BIT .&&. queueFlags q) && (queueCount q > 0)
        graphicsQueueIndices = fromIntegral . fst <$> Vector.filter
          (isGraphicsQueue . snd)
          (Vector.indexed queueFamilyProperties)
    pure graphicsQueueIndices

  getPresentQueueIndices :: MonadIO m => PhysicalDevice -> m (Vector Word32)
  getPresentQueueIndices dev = do
    -- TODO: implement getNum...
    numQueues <- Vector.length <$> Core11.getPhysicalDeviceQueueFamilyProperties2 @'[] dev
    let queueIndices = Vector.generate numQueues fromIntegral
    Vector.filterM
      (\i -> (True ==) <$> getPhysicalDeviceSurfaceSupportKHR dev i surface)
      queueIndices

  getFormat :: MonadIO m => PhysicalDevice -> m SurfaceFormatKHR
  getFormat dev = do
    (_, formats) <- getPhysicalDeviceSurfaceFormatsKHR dev surface
    pure $ case formats of
      [] -> desiredFormat
      [SurfaceFormatKHR FORMAT_UNDEFINED _] -> desiredFormat
      _
        | Vector.any
          (\f ->
            format (f :: SurfaceFormatKHR)
              == format (desiredFormat :: SurfaceFormatKHR)
              && colorSpace (f :: SurfaceFormatKHR)
              == colorSpace (desiredFormat :: SurfaceFormatKHR)
          )
          formats
        -> desiredFormat
      _ -> Vector.head formats

  getPresentMode :: MonadIO m => PhysicalDevice -> MaybeT m PresentModeKHR
  getPresentMode dev = do
    (_, presentModes) <- getPhysicalDeviceSurfacePresentModesKHR dev surface
    let desiredPresentModes =
          [ PRESENT_MODE_MAILBOX_KHR
          , PRESENT_MODE_FIFO_KHR
          , PRESENT_MODE_IMMEDIATE_KHR
          ]
    MaybeT
      . pure
      . headMay
      . Vector.filter (`Vector.elem` presentModes)
      $ desiredPresentModes

----------------------------------------------------------------
-- Debugging
----------------------------------------------------------------

debugUtilsMessengerCreateInfo :: DebugUtilsMessengerCreateInfoEXT
debugUtilsMessengerCreateInfo = zero
  { messageSeverity =
      -- DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT .|.
      -- DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT .|.
      -- DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT .|.
      DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT
  , messageType =
      -- DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT .|.
      DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT .|.
      DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT
  , pfnUserCallback = debugCallbackPtr
  }

foreign import ccall unsafe "DebugCallback.c &debugCallback"
  debugCallbackPtr :: PFN_vkDebugUtilsMessengerCallbackEXT

----------------------------------------------------------------
-- SDL helpers
----------------------------------------------------------------

-- | Run something having initialized SDL
withSDL :: IO a -> IO a
withSDL =
  bracket_
      (SDL.initialize ([SDL.InitEvents, SDL.InitVideo] :: [SDL.InitFlag]))
      SDL.quit
    . bracket_ (vkLoadLibrary Nothing) vkUnloadLibrary

-- | Create an SDL window and use it
withWindow :: Text -> Int -> Int -> (SDL.Window -> IO a) -> IO a
withWindow title width height = bracket
  (SDL.createWindow
    title
    (SDL.defaultWindow
      { SDL.windowInitialSize     = SDL.V2 (fromIntegral width)
                                           (fromIntegral height)
      , SDL.windowGraphicsContext = SDL.VulkanContext
      }
    )
  )
  SDL.destroyWindow

-- | Get the Vulkan surface for an SDL window
getSDLWindowSurface :: Instance -> SDL.Window -> IO SurfaceKHR
getSDLWindowSurface inst window =
  SurfaceKHR <$> vkCreateSurface window (castPtr (instanceHandle inst))

withSDLWindowSurface :: Instance -> SDL.Window -> (SurfaceKHR -> IO a) -> IO a
withSDLWindowSurface inst window = bracket
  (getSDLWindowSurface inst window)
  (\o -> destroySurfaceKHR inst o Nothing)
