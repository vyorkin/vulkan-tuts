#include <stdio.h>
#include <vulkan/vulkan.h>

VKAPI_ATTR VkBool32 VKAPI_CALL
debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
              VkDebugUtilsMessageTypeFlagsEXT messageType,
              const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
              void *pUserData) {
  /* if (messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) */
  int color;

  switch (messageSeverity) {
    case 0x1000:
      color = 91; break;
    case 0x100:
      color = 93; break;
    default:
      color = 37;
  }

  if (color != 37) {
    fprintf(stderr, "\033[%dmValidation (%x): %s\033[m\n", color, messageType, pCallbackData->pMessage);
  } else {
    fprintf(stderr, "Validation (%x): %s\n", messageType, pCallbackData->pMessage);
  }
  fflush(stderr);

  return VK_FALSE;
}
